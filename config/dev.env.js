'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"/dev-api"'
  BASE_API: '"http://127.0.0.1:9013/yhy-web"',
  BASE_ACCESS_URL: '"http://127.0.0.1:8014"'
  // BASE_API: '"http://localhost:8001"'
})
