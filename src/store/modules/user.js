import { login, getUserInfo, logout } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { displayImageUrl } from '@/api/system/fileMng'

const user = {
  state: {
    token: getToken(),
    user: {},
    roles: [],
    // 是否已加载用户信息
    userInfoFlag: false,
    // 第一次加载菜单时用到
    loadMenus: false
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USER: (state, user) => {
      state.user = user
    },
    SET_USERINFO_FLAG: (state, userInfoFlag) => {
      state.userInfoFlag = userInfoFlag
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_LOAD_MENUS: (state, loadMenus) => {
      state.loadMenus = loadMenus
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const userAccount = userInfo.userAccount
      const password = userInfo.password
      const rememberMe = userInfo.rememberMe
      return new Promise((resolve, reject) => {
        login(userAccount, password).then(res => {
          setToken(res.token, rememberMe)
          commit('SET_TOKEN', res.token)
          setUserInfo(res.data, commit)
          commit('SET_USERINFO_FLAG', false)
          // 第一次加载菜单时用到， 具体见 src 目录下的 permission.js
          commit('SET_LOAD_MENUS', true)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetUserInfo({ commit }) {
      return new Promise((resolve, reject) => {
        getUserInfo().then(res => {
          const user = res.data
          if (user.avatar) {
            // 设置头像
            user.avatar = displayImageUrl(user.avatar)
          }
          setUserInfo(user, commit)
          commit('SET_USERINFO_FLAG', true)
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 清除Token
    RemoveToken({ commit }) {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
    },
    // 登出
    LogOut({ commit }, userAccount) {
      return new Promise((resolve, reject) => {
        logout(userAccount).then(res => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
      // resolve()
    },

    updateLoadMenus({ commit }) {
      return new Promise((resolve, reject) => {
        commit('SET_LOAD_MENUS', false)
      })
    }
  }
}

export const setUserInfo = (res, commit) => {
  // 如果没有任何权限，则赋予一个默认的权限，避免请求死循环
  if (res.roles.length === 0) {
    commit('SET_ROLES', [])
  } else {
    commit('SET_ROLES', res.roles)
  }
  commit('SET_USER', res)
}

export default user
