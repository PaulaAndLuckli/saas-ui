import request from '@/utils/request'

export function loadDataUrl() {
  return 'form-service/api/formApplyMng/loadData'
}

export function submitUrl() {
  return 'form-service/api/formApplyMng/doSubmit'
}

export function destoryUrl() {
  return 'form-service/api/formApplyMng/doDestory'
}

export function deleteUrl() {
  return 'form-service/api/formApplyMng/doDelete'
}

export function getApplyFieldValueInfo(mainId) {
  return request({
    url: 'form-service/api/formApplyMng/getApplyFieldValueInfo',
    method: 'get',
    params: {
      mainId
    }
  })
}

export function toViewApprove(data) {
  return request({
    url: 'form-service/api/formApplyMng/toViewApprove',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'form-service/api/formApplyMng/toView',
    method: 'post',
    data
  })
}

export function toAdd(data) {
  return request({
    url: 'form-service/api/formApplyMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'form-service/api/formApplyMng/toCopy',
    method: 'post',
    data
  })
}

export function toChange(data) {
  return request({
    url: 'form-service/api/formApplyMng/toChange',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'form-service/api/formApplyMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'form-service/api/formApplyMng/doDelete',
    method: 'post',
    data
  })
}
